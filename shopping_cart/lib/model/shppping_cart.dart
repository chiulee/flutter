import 'package:flutter/material.dart';
import 'package:shopping_cart/model/product.dart';

class ShoppingCart extends ChangeNotifier {
  List<Product> _cart = <Product>[];
  List<Product> get cart => _cart;

  add(Product item) {
    _cart.add(item);
    notifyListeners();
  }

  remove(Product item) {
    _cart.remove(item);
    notifyListeners();
  }

  removeAll() {
    _cart.clear();
    notifyListeners();
  }
}
