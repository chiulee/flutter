//private public 权限修饰符  _ 私有
//The unnamed constructor is already defined.
//命名的构造函数
class Animal {
  String? name;
  Animal() {
    print('Animal()');
  }
  Animal.initFromName({required this.name});
  void eat() {
    print('${name} is eating!');
  }
}
