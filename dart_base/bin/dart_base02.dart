import 'lib/animal.dart';
import 'lib/cat.dart';

//在继承的时候 ，子类没有继承parent的命名构造函数
void main() {
  Animal a = Animal.initFromName(
    name: 'Simba',
  );
  a.eat();

  Animal c = Cat.initFromName(name: 'tom');
  c.eat();
}
