import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    ScrollController _controller = ScrollController();
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: ListView.builder(
            controller: _controller,
            itemCount: 100,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text('$index'),
              );
            }),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_upward),
          onPressed: () {
            _controller.animateTo(0,
                duration: Duration(microseconds: 200), curve: Curves.easeIn);
          },
        ),
      ),
    );
  }
}

class SingleScrollWidget extends StatelessWidget {
  const SingleScrollWidget({
    Key? key,
    required this.str,
  }) : super(key: key);

  final String str;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      scrollDirection: Axis.horizontal,
      reverse: true,
      child: Center(
          child: Row(
        children: str
            .split('')
            .map((v) => Text(
                  v,
                  textScaleFactor: 3,
                ))
            .toList(),
      )),
    );
  }
}
